//
//  GDTModule.h
//  RNGdt
//
//  Created by liangfengsid on 2020/7/31.
//  Copyright © 2020 Qmaple. All rights reserved.
//

#ifndef GDTModule_h
#define GDTModule_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <React/RCTBridgeModule.h>

// define share type constants

@interface GDTModule : NSObject <RCTBridgeModule>

@property NSString* appId;

@end

#endif /* GDTModule_h */
