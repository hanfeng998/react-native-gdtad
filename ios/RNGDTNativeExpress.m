//
//  RNGDTNativeExpress.m
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "RNGDTNativeExpress.h"
#import "GDTNativeExpressAd.h"
#import "GDTNativeExpressAdView.h"

@interface RNGDTNativeExpress() <GDTNativeExpressAdDelegete>

@property (nonatomic, strong) NSMutableArray *expressAdViews;
@property (nonatomic, strong) GDTNativeExpressAd *nativeExpressAd;

@end

@implementation RNGDTNativeExpress

 - (void)setAppInfo:(NSDictionary *)appInfo {
     NSLog(@"in nativeExpress setAppInfo, posId: %@", appInfo[@"posId"]);
     if (self.nativeExpressAd) {
         self.nativeExpressAd.delegate = nil;
         self.nativeExpressAd = nil;
     }
     CGFloat width = [UIScreen mainScreen].bounds.size.width;
     self.nativeExpressAd = [[GDTNativeExpressAd alloc] initWithPlacementId:appInfo[@"posId"] adSize:CGSizeMake(width, 50)];
     self.nativeExpressAd.delegate = self;
     self.nativeExpressAd.maxVideoDuration = 5;
     self.nativeExpressAd.minVideoDuration = 30;
     self.nativeExpressAd.detailPageVideoMuted = YES;
     self.nativeExpressAd.videoAutoPlayOnWWAN = NO;
     self.nativeExpressAd.videoMuted = YES;
//     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//         NSLog(@"DEBUG: in nativeExpress dispatch");
         [self.nativeExpressAd loadAd:(NSInteger)1];
//     });
 }

#pragma mark - GDTNativeExpressAdDelegete
/**
 * 拉取广告成功的回调
 */
- (void)nativeExpressAdSuccessToLoad:(GDTNativeExpressAd *)nativeExpressAd views:(NSArray<__kindof GDTNativeExpressAdView *> *)views
{
    NSLog(@"%s",__FUNCTION__);
    self.expressAdViews = [NSMutableArray arrayWithArray:views];
    if (self.expressAdViews.count) {
        UIViewController* viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        GDTNativeExpressAdView *adView = (GDTNativeExpressAdView *)[self.expressAdViews objectAtIndex:0];
        adView.controller = viewController;
        [adView render];
        [self addSubview:adView];
        NSLog(@"eCPM:%ld eCPMLevel:%@", [adView eCPM], [adView eCPMLevel]);
        if (self.onLoaded) {
            self.onLoaded(nil);
        }
    }
}

/**
 * 拉取广告失败的回调
 */
- (void)nativeExpressAdFailToLoad:(GDTNativeExpressAd *)nativeExpressAd error:(NSError *)error
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"ERROR: %@", error);
    if (self.onFailToReceived) {
        self.onFailToReceived(@{@"error": error.localizedDescription});
    }
}

- (void)nativeExpressAdViewRenderSuccess:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onSucceeded) {
        self.onSucceeded(nil);
    }
}

- (void)nativeExpressAdRenderFail:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onFailed) {
        self.onFailed(nil);
    }
}

- (void)nativeExpressAdViewClicked:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onClicked) {
        self.onClicked(nil);
    }
}

- (void)nativeExpressAdViewClosed:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    [self.expressAdViews removeObject:nativeExpressAdView];
    [nativeExpressAdView removeFromSuperview];
    if (self.onViewWillClose) {
        self.onViewWillClose(nil);
    }
    
}

- (void)nativeExpressAdViewExposure:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onViewWillExposure) {
        self.onViewWillExposure(nil);
    }
}

- (void)nativeExpressAdViewWillPresentScreen:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onViewWillPresentFullScreenModal) {
        self.onViewWillPresentFullScreenModal(nil);
    }
}

- (void)nativeExpressAdViewDidPresentScreen:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onViewDidPresentFullScreenModal) {
        self.onViewDidPresentFullScreenModal(nil);
    }
}

- (void)nativeExpressAdViewWillDismissScreen:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onViewWillDismissFullScreenModal) {
        self.onViewWillDismissFullScreenModal(nil);
    }
}

- (void)nativeExpressAdViewDidDismissScreen:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onViewDidDismissFullScreenModal) {
        self.onViewDidDismissFullScreenModal(nil);
    }
}

/**
 * 详解:当点击应用下载或者广告调用系统程序打开时调用
 */
- (void)nativeExpressAdViewApplicationWillEnterBackground:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"--------%s-------",__FUNCTION__);
}

- (void)nativeExpressAdView:(GDTNativeExpressAdView *)nativeExpressAdView playerStatusChanged:(GDTMediaPlayerStatus)status
{
    NSLog(@"%s",__FUNCTION__);
    NSLog(@"view:%@ duration:%@ playtime:%@ status:%@ isVideoAd:%@", nativeExpressAdView,@([nativeExpressAdView videoDuration]), @([nativeExpressAdView videoPlayTime]), @(status), @(nativeExpressAdView.isVideoAd));
}

- (void)nativeExpressAdViewWillPresentVideoVC:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"--------%s-------",__FUNCTION__);
}

- (void)nativeExpressAdViewDidPresentVideoVC:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"--------%s-------",__FUNCTION__);
}
- (void)nativeExpressAdViewWillDismissVideoVC:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"--------%s-------",__FUNCTION__);
}
- (void)nativeExpressAdViewDidDismissVideoVC:(GDTNativeExpressAdView *)nativeExpressAdView
{
    NSLog(@"--------%s-------",__FUNCTION__);
}

@end
