//
//  RNGDTNativeExpressViewManager.m
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGDTNativeExpressViewManager.h"
#import "RNGDTNativeExpress.h"

@implementation RNGDTNativeExpressViewManager

// - (dispatch_queue_t)methodQueue {
//     return dispatch_get_main_queue();
// }

 RCT_EXPORT_MODULE(GDTNativeExpress);

 RCT_EXPORT_VIEW_PROPERTY(appInfo, NSDictionary);

 RCT_EXPORT_VIEW_PROPERTY(onLoaded, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onFailToReceived, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onFailed, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onSucceeded, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillLeaveApplication, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillClose, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillExposure, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onClicked, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillPresentFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewDidPresentFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewWillDismissFullScreenModal, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onViewDidDismissFullScreenModal, RCTBubblingEventBlock);

 - (UIView *)view {
     return [[RNGDTNativeExpress alloc] init];
 }

@end
