package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.core.app.ActivityCompat;

import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

import cn.cnlee.commons.gdt.R;

public class Splash extends RelativeLayout {

    private static final String SKIP_TEXT = "点击跳过 %d";
    private ViewGroup container;
    private TextView skipView;
    private View logoView;
    private ImageView splashHolder;

    private SplashAD mSplashAD;
    private Runnable mLayoutRunnable;

    public Splash(Context context, String posID, SplashADListener listener, int fetchDelay) {
        super(context);
        // 把布局加载到这个View里面
        inflate(context, R.layout.layout_splash, this);
        container = findViewById(R.id.splash_container);
        skipView = findViewById(R.id.skip_view);
        skipView.setVisibility(View.VISIBLE);
        logoView = findViewById(R.id.app_logo);
        splashHolder = findViewById(R.id.splash_holder);
        initView(posID, listener, fetchDelay);
    }


    /**
     * 初始化View
     */
    private void initView(String posID, SplashADListener listener, int fetchDelay) {
        if (Build.VERSION.SDK_INT >= 23) {
            checkAndRequestPermission(posID, listener, fetchDelay);
        } else {
            // 如果是Android6.0以下的机器，建议在manifest中配置相关权限，这里可以直接调用SDK
            fetchSplashAD(posID, listener, fetchDelay);
        }
    }

    private void fetchSplashAD(String posId, SplashADListener adListener, int fetchDelay) {
        mSplashAD = new SplashAD((Activity) this.getContext(), this.skipView, posId, adListener, fetchDelay);
        mSplashAD.fetchAndShowIn(this.container);
    }

    public void showLogo(boolean show) {
        if (logoView != null) {
            logoView.setVisibility(show ? VISIBLE : GONE);
        }
    }

    public void hideSplashHolder(boolean hide) {
        if (splashHolder != null) {
            splashHolder.setVisibility(hide ? INVISIBLE : VISIBLE);
        }
    }

    public void setTickTxt(long timeLeft) {
        if (skipView != null) {
            skipView.setText(String.format(Locale.CHINA, SKIP_TEXT, Math.round(timeLeft / 1000f)));
        }
    }

    public void showAd() {
        if (this.mSplashAD != null) {
            this.mSplashAD.showAd(this.container);
        }
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        if (mLayoutRunnable != null) {
            removeCallbacks(mLayoutRunnable);
        }
        mLayoutRunnable = new Runnable() {
            @Override
            public void run() {
                measure(
                        MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
                layout(getLeft(), getTop(), getRight(), getBottom());
            }
        };
        post(mLayoutRunnable);
    }

    /**
     *
     * ----------非常重要----------
     *
     * Android6.0以上的权限适配简单示例：
     *
     * 如果targetSDKVersion >= 23，那么建议动态申请相关权限，再调用广点通SDK
     *
     * SDK不强制校验下列权限（即:无下面权限sdk也可正常工作），但建议开发者申请下面权限，尤其是READ_PHONE_STATE权限
     *
     * READ_PHONE_STATE权限用于允许SDK获取用户标识,
     * 针对单媒体的用户，允许获取权限的，投放定向广告；不允许获取权限的用户，投放通投广告，媒体可以选择是否把用户标识数据提供给优量汇，并承担相应广告填充和eCPM单价下降损失的结果。
     *
     * Demo代码里是一个基本的权限申请示例，请开发者根据自己的场景合理地编写这部分代码来实现权限申请。
     * 注意：下面的`checkSelfPermission`和`requestPermissions`方法都是在Android6.0的SDK中增加的API，如果您的App还没有适配到Android6.0以上，则不需要调用这些方法，直接调用广点通SDK即可。
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void checkAndRequestPermission(String posID, SplashADListener listener, int fetchDelay) {
        List<String> lackedPermission = new ArrayList<String>();
        if (!(ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!(ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        // 快手SDK所需相关权限，存储权限，此处配置作用于流量分配功能，关于流量分配，详情请咨询商务;如果您的APP不需要快手SDK的流量分配功能，则无需申请SD卡权限
        if (!(ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED )){
            lackedPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!(ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            lackedPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // 如果需要的权限都已经有了，那么直接调用SDK
        if (lackedPermission.size() == 0) {
            fetchSplashAD(posID, listener, fetchDelay);
        } else {
            // 否则，建议请求所缺少的权限，在onRequestPermissionsResult中再看是否获得权限
            String[] requestPermissions = new String[lackedPermission.size()];
            lackedPermission.toArray(requestPermissions);
            ActivityCompat.requestPermissions((Activity)this.getContext(), requestPermissions, 1024);
            fetchSplashAD(posID, listener, fetchDelay);
        }
    }
}
